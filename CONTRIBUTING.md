# Contributing to MkDocs

See the contributing guide in the documentation for an
introduction to contributing to MkDocs.

<https://www.mkdocs.org/about/contributing/>

## Usage pour les devs

- Utiliser npm pour copier le DSFR dans le thème custom
    - `npm install`
    - `npm run copy`
- Utiliser pipenv pour gérer les dépendances
- Utiliser pdm pour déployer sur pypi :
    - `pdm build`
    - `pdm publish --nobuild`
