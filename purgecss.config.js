module.exports = {
    content: ['./site/index.html'],
    css: ['./site/dsfr.min.css'],
    output: './dsfr/',
    safelist: [/modal/, /theme/, /checkbox/, /label/]
}
